if ( ! function_exists('hioice_carousel_slide') ) {

// Register Custom Post Type
function hioice_carousel_slide() {

	$labels = array(
		'name'                  => _x( 'Slides', 'Post Type General Name', HIOICE_FBC_DOMAIN ),
		'singular_name'         => _x( 'Slide', 'Post Type Singular Name', HIOICE_FBC_DOMAIN ),
		'menu_name'             => __( 'Carousel Slides', HIOICE_FBC_DOMAIN ),
		'name_admin_bar'        => __( 'Carousel Slide', HIOICE_FBC_DOMAIN ),
		'archives'              => __( 'Item Archives', HIOICE_FBC_DOMAIN ),
		'attributes'            => __( 'Item Attributes', HIOICE_FBC_DOMAIN ),
		'parent_item_colon'     => __( 'Parent Item:', HIOICE_FBC_DOMAIN ),
		'all_items'             => __( 'All Items', HIOICE_FBC_DOMAIN ),
		'add_new_item'          => __( 'Add New Item', HIOICE_FBC_DOMAIN ),
		'add_new'               => __( 'Add New', HIOICE_FBC_DOMAIN ),
		'new_item'              => __( 'New Item', HIOICE_FBC_DOMAIN ),
		'edit_item'             => __( 'Edit Item', HIOICE_FBC_DOMAIN ),
		'update_item'           => __( 'Update Item', HIOICE_FBC_DOMAIN ),
		'view_item'             => __( 'View Item', HIOICE_FBC_DOMAIN ),
		'view_items'            => __( 'View Items', HIOICE_FBC_DOMAIN ),
		'search_items'          => __( 'Search Item', HIOICE_FBC_DOMAIN ),
		'not_found'             => __( 'Not found', HIOICE_FBC_DOMAIN ),
		'not_found_in_trash'    => __( 'Not found in Trash', HIOICE_FBC_DOMAIN ),
		'featured_image'        => __( 'Featured Image', HIOICE_FBC_DOMAIN ),
		'set_featured_image'    => __( 'Set featured image', HIOICE_FBC_DOMAIN ),
		'remove_featured_image' => __( 'Remove featured image', HIOICE_FBC_DOMAIN ),
		'use_featured_image'    => __( 'Use as featured image', HIOICE_FBC_DOMAIN ),
		'insert_into_item'      => __( 'Insert into item', HIOICE_FBC_DOMAIN ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', HIOICE_FBC_DOMAIN ),
		'items_list'            => __( 'Items list', HIOICE_FBC_DOMAIN ),
		'items_list_navigation' => __( 'Items list navigation', HIOICE_FBC_DOMAIN ),
		'filter_items_list'     => __( 'Filter items list', HIOICE_FBC_DOMAIN ),
	);
	$args = array(
		'label'                 => __( 'Slide', HIOICE_FBC_DOMAIN ),
		'description'           => __( 'Carousel slide', HIOICE_FBC_DOMAIN ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => false,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'rewrite'               => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'hioice_slide', $args );

}
add_action( 'init', 'hioice_carousel_slide', 0 );

}