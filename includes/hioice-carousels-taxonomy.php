if ( ! function_exists( 'hioice_carousels' ) ) {

// Register Custom Taxonomy
function hioice_carousels() {

	$labels = array(
		'name'                       => _x( 'Carousels', 'Taxonomy General Name', HIOICE_FBC_DOMAIN ),
		'singular_name'              => _x( 'Carousel', 'Taxonomy Singular Name', HIOICE_FBC_DOMAIN ),
		'menu_name'                  => __( 'Carousel', HIOICE_FBC_DOMAIN ),
		'all_items'                  => __( 'All Items', HIOICE_FBC_DOMAIN ),
		'parent_item'                => __( 'Parent Item', HIOICE_FBC_DOMAIN ),
		'parent_item_colon'          => __( 'Parent Item:', HIOICE_FBC_DOMAIN ),
		'new_item_name'              => __( 'New Item Name', HIOICE_FBC_DOMAIN ),
		'add_new_item'               => __( 'Add New Item', HIOICE_FBC_DOMAIN ),
		'edit_item'                  => __( 'Edit Item', HIOICE_FBC_DOMAIN ),
		'update_item'                => __( 'Update Item', HIOICE_FBC_DOMAIN ),
		'view_item'                  => __( 'View Item', HIOICE_FBC_DOMAIN ),
		'separate_items_with_commas' => __( 'Separate items with commas', HIOICE_FBC_DOMAIN ),
		'add_or_remove_items'        => __( 'Add or remove items', HIOICE_FBC_DOMAIN ),
		'choose_from_most_used'      => __( 'Choose from the most used', HIOICE_FBC_DOMAIN ),
		'popular_items'              => __( 'Popular Items', HIOICE_FBC_DOMAIN ),
		'search_items'               => __( 'Search Items', HIOICE_FBC_DOMAIN ),
		'not_found'                  => __( 'Not Found', HIOICE_FBC_DOMAIN ),
		'no_terms'                   => __( 'No items', HIOICE_FBC_DOMAIN ),
		'items_list'                 => __( 'Items list', HIOICE_FBC_DOMAIN ),
		'items_list_navigation'      => __( 'Items list navigation', HIOICE_FBC_DOMAIN ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'query_var'                  => 'carousel_name',
		'rewrite'                    => false,
	);
	register_taxonomy( 'hioice_carousel', array( 'hioice_slide' ), $args );

}
add_action( 'init', 'hioice_carousels', 0 );

}