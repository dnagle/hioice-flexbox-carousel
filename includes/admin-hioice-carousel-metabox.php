/**
 * Add meta box
 *
 * @param post $post The post object
 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/add_meta_boxes
 */
function add_meta_boxes_hioice_slide( $post ) {
	add_meta_box(
		'hioice_slide_url_meta_box',
		__( 'Slide URL', HIOICE_FBC_DOMAIN ),
		'hioice_build_slide_url_meta_box',
		'hioice_slide',
		'normal',
		'low' );
}
add_action( 'add_meta_boxes', 'add_meta_boxes_hioice_slide' );

function hioice_slide_url_build_meta_box( $post ) {

	wp_nonce_field( basename( __FILE__ ), 'hioice_slide_nonce' );

	$hioice_slide_stored_meta = get_post_meta( $post->ID );
?>
<p>
<label for="hioice-slide-url" class="hioice-slide-row-title"><?php _e( 'Slide URL', HIOICE_FBC_DOMAIN )?></label>
<input type="text" name="hioice-slide-url" id="hioice-slide-url" value="<?php if ( isset ( $hioice_slide_stored_meta['hioice-slide-url'] ) ) echo $hioice_slide_stored_meta['hioice-slide-url'][0]; ?>" />
</p>
<?php
}

/**
 * Saves the custom meta input
 */
 function hioice_slide_meta_save( $post_id ) {
	
		 // Checks save status
		 $is_autosave = wp_is_post_autosave( $post_id );
		 $is_revision = wp_is_post_revision( $post_id );
		 $is_valid_nonce = ( isset( $_POST[ 'hioice_slide_nonce' ] ) && wp_verify_nonce( $_POST[ 'hioice_slide_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
	
		 // Exits script depending on save status
		 if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
				 return;
		 }
	
		 // Checks for input and sanitizes/saves if needed
		 if( isset( $_POST[ 'hioice-slide-url' ] ) ) {
				 update_post_meta( $post_id, 'hioice-slide-url', sanitize_text_field( $_POST[ 'hioice-slide-url' ] ) );
		 }
	
 }
 add_action( 'save_post', 'hioice_slide_meta_save' );