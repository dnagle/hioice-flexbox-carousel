/**
 * Plugin Name: HIOICE Flexbox Carousel 
 * Plugin URI: 
 * Description: A simple plugin that adds flexbox powered carousels.
 * Version: 1.0
 * Author: Dan Nagle 
 * Text Domain: hioice-flexbox-carousel
 * Domain Path: /languages/
 * License: GPLv2 
 * Bitbucket Plugin URI: https://bitbucket.org/dnagle/hioice-flexbox-carousel
 *
 * @package WordPress
 * @author Dan Nagle
 */

if ( !defined( 'HIOICE_FBC_VERSION' ) ) {
	define( 'HIOICE_FBC_VERSION', '1.0.0' ); // Plugin version
}
if ( !defined( 'HIOICE_FBC_DIR' ) ) {
	define( 'HIOICE_FBC_DIR', dirname( __FILE__ ) ); // Plugin directory
}
if ( !defined( 'HIOICE_FBC_URL' ) ) {
	define( 'HIOICE_FBC_URL', plugin_dir_url( __FILE__ ) ); // Plugin url
}
if ( !defined( 'HIOICE_FBC_DOMAIN' ) ) {
	define( 'HIOICE_FBC_DOMAIN', 'hioice-flexbox-carousel' ); // Text Domain
}

/**
 * Load Text Domain
 * This gets the plugin ready for translation
 * 
 * @package Flexbox Carousel
 * @since 1.0.0
 */
function hioice_fbc_load_textdomain() {
	load_plugin_textdomain( HIOICE_FBC_DOMAIN, false, HIOICE_FBC_DIR . '/languages/' );
}
add_action('plugins_loaded', 'hioice_fbc_load_textdomain');

require_once HIOICE_FBC_DIR . '/includes/hioice-carousel-slide-cpt.php';

require_once HIOICE_FBC_DIR . '/includes/hioice-carousels-taxonomy.php';

if ( is_admin() ) {

	require_once HIOICE_FBC_DIR . '/includes/admin-hioice-carousel-metabox.php';

}

function hioice_fbc_scripts() {
	wp_register_script( 'flexbox.carousel', HIOICE_FBC_URL . '/js/flexbox-carousel.js', array(), null, true );
}
add_action( 'wp_register_scripts', 'hioice_fbc_scripts' );

function hioice_fbc_styles() {
	wp_register_style( 'flexbox-carousel', HIOICE_FBC_URL . '/css/flexbox-carousel.css', array(), null, 'all' );
	wp_enqueue_style('flexbox-carousel');
}
add_action( 'wp_enqueue_styles', 'hioice_fbc_styles' );

/**
 * Locate template.
 *
 * Locate the called template.
 * Search Order:
 * 1. /themes/theme/templates/$template_name
 * 2. /themes/theme/$template_name
 * 3. /plugins/plugin/templates/$template_name.
 *
 * @since 1.0.0
 *
 * @param   string  $template_name          Template to load.
 * @param   string  $string $template_path  Path to templates.
 * @param   string  $default_path           Default path to template files.
 * @return  string                          Path to the template file.
 */
function hioice_fbc_locate_template( $template_name, $template_path = '', $default_path = '' ) {

  // Set variable to search in the templates folder of theme.
  if ( ! $template_path ) :
    $template_path = 'templates/';
  endif;

  // Set default plugin templates path.
  if ( ! $default_path ) :
    $default_path = plugin_dir_path( __FILE__ ) . 'templates/'; // Path to the template folder
  endif;

  // Search template file in theme folder.
  $template = locate_template( array(
    $template_path . $template_name,
    $template_name
  ) );

  // Get plugins template file.
  if ( ! $template ) :
    $template = $default_path . $template_name;
  endif;

  return apply_filters( 'hioice_fbc_locate_template', $template, $template_name, $template_path, $default_path );

}

/**
 * Get template.
 *
 * Search for the template and include the file.
 *
 * @since 1.0.0
 *
 * @see hioice_fbc_locate_template()
 *
 * @param string  $template_name          Template to load.
 * @param array   $args                   Args passed for the template file.
 * @param string  $string $template_path  Path to templates.
 * @param string  $default_path           Default path to template files.
 */
function hioice_fbc_get_template( $template_name, $args = array(), $tempate_path = '', $default_path = '' ) {

  if ( is_array( $args ) && isset( $args ) ) :
    extract( $args );
  endif;

  $template_file = hioice_fbc_locate_template( $template_name, $tempate_path, $default_path );

  if ( ! file_exists( $template_file ) ) :
    _doing_it_wrong( __FUNCTION__, sprintf( '<code>%s</code> does not exist.', $template_file ), '1.0.0' );
    return;
  endif;

  include $template_file;

}

/**
 * Template loader.
 *
 * The template loader will check if WP is loading a template
 * for a specific Post Type and will try to load the template
 * from our 'templates' directory.
 *
 * @since 1.0.0
 *
 * @param string  $template Template file that is being loaded.
 * @return  string          Template file that should be loaded.
 */
function hioice_fbc_template_loader( $template ) {

  $find = array();
  $file = '';

  if( is_singular() ):
    $file = 'single-plugin.php';
  elseif( is_tax() ):
    $file = 'archive-plugin.php';
  endif;

  if ( file_exists( hioice_fbc_locate_template( $file ) ) ) :
    $template = hioice_fbc_locate_template( $file );
  endif;

  return $template;

}
add_filter( 'template_include', 'hioice_fbc_template_loader' );

