<div class='carousel-container'>
  <div class='carousel-controls'>
    <button class='carousel-btn carousel-prev'>Prev</button>
  </div>
  <div class='carousel-wrap'>
    <ul class='carousel has-animated'>
      <li class='carousel-slide'>
        <h2>abc</h2>
      </li>
      <li class='carousel-slide'>
        <h2>def</h2>
      </li>
      <li class='carousel-slide'>
        <h2>ghi</h2>
      </li>
      <li class='carousel-slide'>
        <h2>123</h2>
      </li>
      <li class='carousel-slide'>
        <h2>456</h2>
      </li>
      <li class='carousel-slide reference'>
        <h2>789</h2>
      </li>
    </ul>
  </div>
  <div class='carousel-controls'>
    <button class='carousel-btn carousel-next' data-toggle='next'>Next</button>
  </div>
</div>