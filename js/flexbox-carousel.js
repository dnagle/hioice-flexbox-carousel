(function(document, undefined) {
    'use strict';
    var addClass, hasClass, nextSlide, prevSlide, removeClass, slideAction;

    hasClass = function(el, className) {
        return (el.classList) ? el.classList.contains(className) : new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
    };

    addClass = function(el, className) {
        if (el.classList) {
            el.classList.add(className);
        } else {
            el.className += ' ' + className;
        }
    };

    removeClass = function(el, className) {
        if (el.classList) {
            el.classList.remove(className);
        } else {
            el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        }
    };

    nextSlide = function(el) {
        if (el.nextElementSibling) {
            return el.nextElementSibling;
        } else {
            return el.parentNode.children[0];
        }
    };

    prevSlide = function(el) {
        if (el.previousElementSibling) {
            return el.previousElementSibling;
        } else {
            return el.parentNode.children[el.parentNode.childElementCount - 1];
        }
    };

    slideAction = function(carousel, direction) {

        var asc, el, end, i, slide, slides;

        slides = carousel.querySelectorAll('.carousel-slide');
        el = carousel.querySelector('.reference');
        removeClass(el, 'reference');

        if (direction === 'next') {
            slide = nextSlide(el);
            removeClass(carousel, 'reverse');
        } else {
            slide = prevSlide(el);
            addClass(carousel, 'reverse');
        }

        addClass(slide, 'reference');

        slide.style.order = 1;

        for (i = 2, end = slides.length, asc = 2 <= end; asc ? i <= end : i >= end; asc ? i++ : i--) {
            slide = nextSlide(slide);
            slide.style.order = i;
        }

        removeClass(carousel, 'has-animated');

        return setTimeout(function() {
            addClass(carousel, 'has-animated');
        }, 100);

    };

    document.addEventListener('click', function(e) {

        var carousel, direction;

        if (hasClass(e.target, 'carousel-btn')) {

            carousel = e.target.parentNode.parentNode.querySelector('.carousel');

            direction = (hasClass(e.target, 'carousel-next')) ? 'next' : 'prev';

            slideAction(carousel, direction);

        }

    });

})(document);